__author__ = 'Rok_in_Sebastjan '

from Paketi import *
from Havlas_funkcije import boljsi_poberi_kodo_zaporedja1, poberi_nakljucno



class Zacetno():

    def iskanje(self, *args):
        '''
        Funkcija preveri pravilnost vpisa in ustreznost podatkov iz spletne strani, ter v ustreznem primeru začne spremembo
        okna
        '''

        if self.vnos.get() == "":
            messagebox.showwarning("Napaka", "Niste vpisali zaporedja!")

        elif boljsi_poberi_kodo_zaporedja1(self.vnos.get())=="slabo":
                messagebox.showwarning("Napaka", "Ni bilo mogoče najti nobenega zaporedja.")
        elif boljsi_poberi_kodo_zaporedja1(self.vnos.get())=="preveč":
                messagebox.showwarning("Napaka", "Iskanje vrne preveliko število rezultatov. Prosim, omeji iskanje.")

        else:

            self.vnos = self.vnos.get()
            self.zamenjaj = True



    def nakljucno(self, *args):
        '''
        Realizira naključno iskanje.
        '''

        self.zamenjaj = True
        self.vnos = self.naklj

    def __init__(self, root):

        root.title("Iskanje zaporedja")


        #
        #   Definicija atributov začetnega okna, ki bodo signalizirala spremembo in prenesla uporabnikov vnos v
        #   glavno okno.
        #

        self.zamenjaj = False
        self.vnos = StringVar()
        self.naklj = 'A11'
        while len(self.naklj) < 7:
            self.naklj += str(randint(0,9))

        #
        #   Osnovna postavitev
        #

        zgOkv = Frame(root, padx = 5, pady = 5)
        srOkv = Frame(root, padx = 5, pady = 5)
        spOkv = Frame(root, padx = 5, pady = 5)
        zgOkv.pack(side = TOP, fill = BOTH, expand = True)
        spOkv.pack(side = BOTTOM, fill = BOTH, expand = True)
        srOkv.pack(fill = BOTH, expand = True)
        zgOkv.configure(background = barva_ozd)
        spOkv.configure(background = barva_ozd)
        srOkv.configure(background = barva_ozd)

        #
        #   Naslov in navodilo
        #

        txt = Label(srOkv, text = navodilo, font = ("Helvetica", 16), bg = barva_ozd)
        title = Label(zgOkv, text = naslov, font = ("Helvetica", 22), bg = barva_ozd)
        txt.pack(side = TOP)
        title.pack()

        #
        #   Vnosna vrstica
        #

        vnosno = Entry(srOkv, textvariable = self.vnos)
        vnosno.focus()
        vnosno.pack(side = BOTTOM, expand = True, fill = BOTH)


        self.isci = ttk.Button(spOkv, text = "Išči", command = self.iskanje)
        self.nak = ttk.Button(spOkv, text = "Naključno zaporedje", command = self.nakljucno)

        self.isci.pack(side = LEFT)
        self.nak.pack(side = RIGHT)

        root.bind('<Return>', self.iskanje)
        txt.pack(side = TOP)
        vnosno.pack(side = BOTTOM, expand = True, fill = BOTH)
        title.pack()

